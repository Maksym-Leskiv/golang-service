#!/bin/bash
BASE_DIR=$(cd $(dirname $0) && pwd)
BUILD_STAGE=${BUILD_STAGE:-$1}
if [[ -z "${BUILD_STAGE}" ]]; then
  echo "stage argument or BUILD_STAGE is missing"
  exit 1
fi
deploy() {
    cd $1
    sls deploy --verbose --stage $BUILD_STAGE
}
deploy "$BASE_DIR"/test
deploy "$BASE_DIR"/service