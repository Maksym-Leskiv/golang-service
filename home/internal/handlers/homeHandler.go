package handlers

import (
	"encoding/json"
	"errors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go-v2/aws"
	log "github.com/sirupsen/logrus"
	"homeservice/internal/clients/cloudWatch"
	"homeservice/internal/clients/dynamodb"
	"homeservice/internal/common"
	"homeservice/internal/converter"
	"homeservice/internal/handlers/apiresponse"
	"homeservice/internal/models"
	"net/http"
)

type HomeHandler struct {
	Table      *dynamodb.HomeTable
	CloudWatch cloudWatch.CloudWatchClient
}

func (h HomeHandler) GetHome(req events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	idStr := req.PathParameters["uuid"]

	if idStr == "" {
		return apiresponse.ApiResponse(http.StatusBadRequest, common.ErrorBody{
			aws.String(apiresponse.ParameterIsMissing),
		})
	}

	result, err := h.Table.GetHome(idStr)

	if err != nil {
		return apiresponse.ApiResponse(http.StatusBadRequest, common.ErrorBody{aws.String(apiresponse.ErrorRecordNotFound)})
	}
	return apiresponse.ApiResponse(http.StatusOK, result)
}

func (h HomeHandler) CreateHome(req events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	var home models.Home

	if err := json.Unmarshal([]byte(req.Body), &home); err != nil {
		return nil, errors.New(apiresponse.ErrorInvalidId)
	}

	result, err := h.Table.CreateHome(converter.ConvertHome(home))

	if err != nil {
		log.Error("Error:", err)
		return apiresponse.ApiResponse(http.StatusBadRequest, common.ErrorBody{
			aws.String(apiresponse.BadRequest),
		})
	}

	h.CloudWatch.ReportNewHomeMetric()

	return apiresponse.ApiResponse(http.StatusCreated, result)
}

func (h HomeHandler) UnhandledMethod() (*events.APIGatewayProxyResponse, error) {
	return apiresponse.ApiResponse(http.StatusMethodNotAllowed, common.ErrorMethodNotAllowed)
}
