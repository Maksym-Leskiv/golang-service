package handlers

import (
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	log "github.com/sirupsen/logrus"
	"homeservice/internal/clients/cloudWatch"
	"homeservice/internal/clients/dynamodb"
	"homeservice/internal/clients/sns"
	"homeservice/internal/utils"
	"testing"
)

var dataForCreate = map[string]interface{}{
	"user_id": "27d0826e-82e0-11ee-b962-0242ac120003",
	"location": map[string]interface{}{
		"lat":      0,
		"lon":      0,
		"country":  "Ukraine",
		"city":     "LVIV",
		"postcode": "1",
		"address":  "street",
	},
	"profile": map[string]interface{}{
		"year":     1999,
		"area":     1,
		"bedrooms": 3,
	},
}

var dataForUpdate = map[string]interface{}{
	"user_id": "27d0826e-82e0-11ee-b962-0242ac120003",
	"location": map[string]interface{}{
		"city":     "ODESA",
		"postcode": "000",
		"address":  "vyl",
	},
	"profile": map[string]interface{}{
		"year": 2000,
	},
}

func initDataForAdminHomeHandler(data map[string]interface{}) events.APIGatewayProxyRequest {

	jsonData, err := json.Marshal(data)
	if err != nil {
		log.Fatal(err)
	}

	request := events.APIGatewayProxyRequest{
		Body: string(jsonData),
	}

	return request
}

func initAdminHomeHandler(t *testing.T) AdminHomeHandler {
	tableName := utils.InitTable(t)
	table := dynamodb.NewHomeTableAWS(utils.AwsConfig(), tableName)
	handler := AdminHomeHandler{
		Table:      table,
		Sns:        sns.SnsMock{},
		CloudWatch: cloudWatch.CloudWatchMock{},
	}
	return handler
}

func TestAdminHomeHandler_UpdateHome(t *testing.T) {
	handler := initAdminHomeHandler(t)
	req := initDataForAdminHomeHandler(dataForCreate)
	req.HTTPMethod = "POST"
	result, err := handler.CreateHome(req)
	if err != nil {
		t.Errorf("Error calling CreateHome: %v", err)
	}
	var responseMap map[string]interface{}
	if err := json.Unmarshal([]byte(result.Body), &responseMap); err != nil {
		t.Errorf("Error unmarshaling JSON response: %v", err)
	}
	uuid, ok := responseMap["UUID"].(string)
	if !ok {
		t.Errorf("UUID field not found")
	}
	req = initDataForAdminHomeHandler(dataForUpdate)
	req.HTTPMethod = "PUT"
	req.PathParameters = map[string]string{
		"uuid": uuid,
	}
	result, err = handler.UpdateHome(req)
	testCases := []struct {
		Description string
		Event       events.APIGatewayProxyRequest
		Expected    events.APIGatewayProxyResponse
	}{{
		Description: "Test of Update Admin Home endpoint",
		Event:       req,
		Expected: events.APIGatewayProxyResponse{
			StatusCode: 200,
			Body:       `{"UUID":"<<PRESENCE>>","UserID":"27d0826e-82e0-11ee-b962-0242ac120003","Profile":{"Year":2000,"Area":1,"Bedrooms":3},"Location":{"Lat":0,"Lon":0,"Country":"Ukraine","City":"ODESA","Postcode":"000","Address":"vyl"},"Created":"<<PRESENCE>>","Updated":"<<PRESENCE>>"}`,
		},
	}}

	utils.AssertTestCase(t, handler.UpdateHome, testCases)
}

func TestAdminHomeHandler_RemoveHome(t *testing.T) {
	handler := initAdminHomeHandler(t)
	req := events.APIGatewayProxyRequest{
		PathParameters: map[string]string{
			"uuid": "b5f3ac7a-838e-11ee-b962-0242ac120002",
		},
		HTTPMethod: "DELETE",
	}
	_, err := handler.RemoveHome(req)
	if err != nil {
		t.Errorf("Error calling GetHome: %v", err)
	}
	testCases := []struct {
		Description string
		Event       events.APIGatewayProxyRequest
		Expected    events.APIGatewayProxyResponse
	}{{
		Description: "Test of DELETE Admin Home endpoint",
		Event:       req,
		Expected: events.APIGatewayProxyResponse{
			StatusCode: 200,
			Body:       `null`,
		},
	}}

	utils.AssertTestCase(t, handler.RemoveHome, testCases)
}
