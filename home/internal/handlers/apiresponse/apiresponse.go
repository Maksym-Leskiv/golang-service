package apiresponse

import (
	"encoding/json"

	"github.com/aws/aws-lambda-go/events"
)

const (
	ErrorFailedToUnmarshalRecord = "failed to unmarshal record"
	ErrorInvalidId               = "invalid id"
	ErrorCouldNotMarshalItem     = "could not marshal item"
	ErrorCouldNotDeleteItem      = "could not delete item"
	ErrorCouldNotDynamoPutItem   = "could not put item in to dynamoDB"
	ErrorRecordNotFound          = "Record not found"
	ParameterIsMissing           = "ID parameter is missing"
	BadRequest                   = "BadRequest"
)

func ApiResponse(status int, body interface{}) (*events.APIGatewayProxyResponse, error) {
	resp := events.APIGatewayProxyResponse{Headers: map[string]string{"Content-Type": "application/json"}}
	resp.StatusCode = status

	stringBody, _ := json.Marshal(body)
	resp.Body = string(stringBody)
	return &resp, nil
}
