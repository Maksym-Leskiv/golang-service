package handlers

import (
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	log "github.com/sirupsen/logrus"
	"homeservice/internal/clients/cloudWatch"
	"homeservice/internal/clients/dynamodb"
	"homeservice/internal/utils"
	"testing"
)

func initDataForHomeHandler() events.APIGatewayProxyRequest {
	data := map[string]interface{}{
		"user_id": "27d0826e-82e0-11ee-b962-0242ac120002",
		"location": map[string]interface{}{
			"lat":      0,
			"lon":      0,
			"country":  "Ukraine",
			"city":     "Kiev",
			"postcode": "100000",
			"address":  "street",
		},
		"profile": map[string]interface{}{
			"year":     1970,
			"area":     57,
			"bedrooms": 3,
		},
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		log.Fatal(err)
	}

	request := events.APIGatewayProxyRequest{
		Body: string(jsonData),
	}

	return request
}

func initHomeHandler(t *testing.T) HomeHandler {
	tableName := utils.InitTable(t)
	table := dynamodb.NewHomeTableAWS(utils.AwsConfig(), tableName)
	handler := HomeHandler{
		Table:      table,
		CloudWatch: cloudWatch.CloudWatchMock{},
	}
	return handler
}

func TestHomeHandler_CreateHome(t *testing.T) {
	handler := initHomeHandler(t)
	req := initDataForHomeHandler()
	req.HTTPMethod = "POST"
	_, err := handler.CreateHome(req)
	if err != nil {
		t.Errorf("Error calling CreateHome: %v", err)
	}
	testCases := []struct {
		Description string
		Event       events.APIGatewayProxyRequest
		Expected    events.APIGatewayProxyResponse
	}{{
		Description: "Test of POST creation Home endpoint",
		Event:       req,
		Expected: events.APIGatewayProxyResponse{
			StatusCode: 201,
			Body:       `{"UUID":"<<PRESENCE>>","UserID":"27d0826e-82e0-11ee-b962-0242ac120002","Profile":{"Year":1970,"Area":57,"Bedrooms":3},"Location":{"Lat":0,"Lon":0,"Country":"Ukraine","City":"Kiev","Postcode":"100000","Address":"street"},"Created":"<<PRESENCE>>","Updated":null}`,
		},
	}}

	utils.AssertTestCase(t, handler.CreateHome, testCases)

}

func TestHomeHandler_GetHome(t *testing.T) {
	handler := initHomeHandler(t)
	req := events.APIGatewayProxyRequest{
		PathParameters: map[string]string{
			"uuid": "b5f3ac7a-838e-11ee-b962-0242ac120002",
		},
		HTTPMethod: "GET",
	}
	_, err := handler.GetHome(req)
	if err != nil {
		t.Errorf("Error calling GetHome: %v", err)
	}
	testCases := []struct {
		Description string
		Event       events.APIGatewayProxyRequest
		Expected    events.APIGatewayProxyResponse
	}{{
		Description: "Test of GET Home endpoint",
		Event:       req,
		Expected: events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body:       `{"error":"Record not found"}`,
		},
	}}

	utils.AssertTestCase(t, handler.GetHome, testCases)
}
