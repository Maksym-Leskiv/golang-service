package handlers

import (
	"encoding/json"
	"errors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go-v2/aws"
	log "github.com/sirupsen/logrus"
	"homeservice/internal/clients/cloudWatch"
	"homeservice/internal/clients/dynamodb"
	"homeservice/internal/clients/sns"
	"homeservice/internal/common"
	"homeservice/internal/converter"
	"homeservice/internal/handlers/apiresponse"
	"homeservice/internal/models"

	"net/http"
)

type AdminHomeHandler struct {
	Sns        sns.SnsClient
	Table      *dynamodb.HomeTable
	CloudWatch cloudWatch.CloudWatchClient
}

func (h AdminHomeHandler) GetHomeById(req events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	idStr := req.PathParameters["uuid"]

	if idStr == "" {
		return apiresponse.ApiResponse(http.StatusBadRequest, common.ErrorBody{
			aws.String(apiresponse.ErrorInvalidId),
		})
	}
	result, err := h.Table.GetHome(idStr)

	if err != nil {
		return apiresponse.ApiResponse(http.StatusNotFound, common.ErrorBody{aws.String(apiresponse.ErrorRecordNotFound)})
	}
	return apiresponse.ApiResponse(http.StatusOK, result)
}

func (h AdminHomeHandler) CreateHome(req events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	var home models.Home

	if err := json.Unmarshal([]byte(req.Body), &home); err != nil {
		return nil, errors.New(apiresponse.ErrorInvalidId)
	}

	result, err := h.Table.CreateHome(converter.ConvertHome(home))

	if err != nil {
		log.Error("Error:", err)
		return apiresponse.ApiResponse(http.StatusBadRequest, common.ErrorBody{
			aws.String(err.Error()),
		})
	}

	h.Sns.PublishNotification(home)

	h.CloudWatch.ReportNewHomeMetric()

	return apiresponse.ApiResponse(http.StatusCreated, result)
}

func (h AdminHomeHandler) UpdateHome(req events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	var home models.Home
	idStr := req.PathParameters["uuid"]
	if err := json.Unmarshal([]byte(req.Body), &home); err != nil {
		return nil, err
	}
	result, err := h.Table.UpdateHome(idStr, converter.ConvertHome(home))
	if err != nil {
		return apiresponse.ApiResponse(http.StatusBadRequest, common.ErrorBody{
			aws.String(err.Error()),
		})
	}

	h.Sns.PublishNotification(home)

	return apiresponse.ApiResponse(http.StatusOK, result)
}

func (h AdminHomeHandler) RemoveHome(req events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	idStr := req.PathParameters["uuid"]

	if idStr == "" {
		return apiresponse.ApiResponse(http.StatusBadRequest, nil)
	} else {
		err := h.Table.RemoveHome(idStr)
		if err != nil {
			return apiresponse.ApiResponse(http.StatusBadRequest, common.ErrorBody{
				aws.String(err.Error()),
			})
		}
		return apiresponse.ApiResponse(http.StatusOK, nil)
	}
}

func (h AdminHomeHandler) UnhandledMethod() (*events.APIGatewayProxyResponse, error) {
	return apiresponse.ApiResponse(http.StatusMethodNotAllowed, common.ErrorMethodNotAllowed)
}
