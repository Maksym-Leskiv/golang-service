package dynamodb

import (
	"context"
	"errors"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"homeservice/internal/handlers/apiresponse"
	"time"
)

type HomeTable struct {
	client    *dynamodb.Client
	tableName string
}

type Home struct {
	UUID     string    `dynamodbav:"uuid"`
	UserID   string    `dynamodbav:"userid"`
	Profile  *Profile  `dynamodbav:"profile,omitempty"`
	Location *Location `dynamodbav:"location,omitempty"`
	Created  int64     `dynamodbav:"created"`
	Updated  *int64    `dynamodbav:"updated"`
}

type Profile struct {
	Year     int `dynamodbav:"year"`
	Area     int `dynamodbav:"area"`
	Bedrooms int `dynamodbav:"bedrooms"`
}

type Location struct {
	Lat      int    `dynamodbav:"lat"`
	Lon      int    `dynamodbav:"lon"`
	Country  string `dynamodbav:"country"`
	City     string `dynamodbav:"city"`
	Postcode string `dynamodbav:"postcode"`
	Address  string `dynamodbav:"address"`
}

func NewHomeTable(cfg aws.Config, tableName string) (*HomeTable, error) {
	client := dynamodb.NewFromConfig(cfg)

	return &HomeTable{
		client:    client,
		tableName: tableName,
	}, nil
}

func NewHomeTableAWS(config aws.Config, table string) *HomeTable {
	return &HomeTable{
		client:    dynamodb.NewFromConfig(config),
		tableName: table,
	}
}

func (t HomeTable) GetHome(id string) (*Home, error) {
	idValue, err := attributevalue.Marshal(id)
	if err != nil {
		log.Error(err)
		return nil, err
	}

	getItemInput := &dynamodb.GetItemInput{
		Key:       map[string]types.AttributeValue{"uuid": idValue},
		TableName: aws.String(t.tableName),
	}

	result, err := t.client.GetItem(context.TODO(), getItemInput)
	if err != nil {
		log.Error(err)
		return nil, errors.New(apiresponse.ErrorRecordNotFound)
	}

	if result.Item == nil || len(result.Item) == 0 {
		return nil, errors.New(apiresponse.ErrorRecordNotFound)
	}

	var item Home

	if err := attributevalue.UnmarshalMap(result.Item, &item); err != nil {
		log.Error(err)
		return nil, errors.New(apiresponse.ErrorFailedToUnmarshalRecord)
	}

	return &item, nil
}

func (t HomeTable) CreateHome(home Home) (*Home, error) {

	id := uuid.New().String()
	currentTime := time.Now().Unix()
	home.UUID = id
	home.Created = currentTime

	marshalItem, err := attributevalue.MarshalMap(&home)

	if err != nil {
		log.Error(err)
		return nil, errors.New(apiresponse.ErrorCouldNotMarshalItem)
	}

	putItemInput := &dynamodb.PutItemInput{
		TableName: aws.String(t.tableName),
		Item:      marshalItem,
	}

	_, err = t.client.PutItem(context.TODO(), putItemInput)
	if err != nil {
		log.Error("Error:", err)
		return nil, errors.New(apiresponse.ErrorCouldNotDynamoPutItem)
	}

	return &home, nil
}

func (t HomeTable) UpdateHome(uuid string, home Home) (*Home, error) {
	homeToUpdate, err := t.GetHome(uuid)
	if err != nil {
		log.Error(err)
		return nil, errors.New(apiresponse.ErrorRecordNotFound)
	}

	currentTime := time.Now().Unix()
	homeToUpdate.Updated = &currentTime

	key := map[string]types.AttributeValue{
		"uuid": &types.AttributeValueMemberS{Value: uuid},
	}

	updateExpression, expressionValues, names := BuildUpdateExpression(home, homeToUpdate)

	updateInput := &dynamodb.UpdateItemInput{
		TableName:                 aws.String(t.tableName),
		Key:                       key,
		UpdateExpression:          aws.String(updateExpression),
		ExpressionAttributeValues: expressionValues,
		ExpressionAttributeNames:  names,
		ReturnValues:              types.ReturnValueNone,
	}

	_, err = t.client.UpdateItem(context.TODO(), updateInput)
	if err != nil {
		log.Error(err)
		return nil, errors.New(apiresponse.ErrorCouldNotDynamoPutItem)
	}

	return homeToUpdate, nil
}

func (t HomeTable) RemoveHome(id string) error {

	idValue, err := attributevalue.Marshal(id)

	if err != nil {
		log.Error(err)
		return err
	}

	deleteItemInput := &dynamodb.DeleteItemInput{
		TableName: aws.String(t.tableName),
		Key:       map[string]types.AttributeValue{"uuid": idValue},
	}

	_, err = t.client.DeleteItem(context.TODO(), deleteItemInput)
	if err != nil {
		log.Error(err)
		return errors.New(apiresponse.ErrorCouldNotDeleteItem)
	}

	return err
}
