package dynamodb

import (
	"errors"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"github.com/samber/lo"
	log "github.com/sirupsen/logrus"
	"strings"
)

func BuildUpdateExpression(updateFields Home, existingFields *Home) (string, map[string]types.AttributeValue, map[string]string) {
	expressions := make([]string, 0)
	values := make(map[string]types.AttributeValue)
	names := make(map[string]string)
	err := errors.New("")

	updateFieldsFlat := flattenHome(updateFields, existingFields)

	for key, value := range updateFieldsFlat {
		if value != nil {
			placeholder := "#" + key
			expressions = append(expressions, fmt.Sprintf("%s = :%s", placeholder, key))
			values[":"+key], err = attributevalue.Marshal(value)
			names[placeholder] = key
		}
		if err != nil {
			log.Error(err)
		}
	}

	updateExpression := "SET " + strings.Join(expressions, ", ")

	return updateExpression, values, names
}

func flattenHome(updates Home, existing *Home) map[string]interface{} {
	result := make(map[string]interface{})

	//result["uuid"] = updates.UUID
	result["userid"] = updates.UserID
	result["profile"] = flattenProfile(updates.Profile, existing.Profile)
	result["location"] = flattenLocation(updates.Location, existing.Location)
	result["created"] = updates.Created
	result["updated"] = updates.Updated

	return result
}

func flattenProfile(updateProfile *Profile, exProfile *Profile) map[string]interface{} {
	if updateProfile == nil {
		return nil
	}

	result := make(map[string]interface{})

	if lo.IsNotEmpty(updateProfile.Year) {
		result["year"] = updateProfile.Year
	} else {
		result["year"] = exProfile.Year
	}

	if lo.IsNotEmpty(updateProfile.Area) {
		result["area"] = updateProfile.Area
	} else {
		result["area"] = exProfile.Area
	}

	if lo.IsNotEmpty(updateProfile.Bedrooms) {
		result["bedrooms"] = updateProfile.Bedrooms
	} else {
		result["bedrooms"] = exProfile.Bedrooms
	}

	return result
}

func flattenLocation(updateLocation *Location, exLocation *Location) map[string]interface{} {
	if updateLocation == nil {
		return nil
	}

	result := make(map[string]interface{})

	if lo.IsNotEmpty(updateLocation.Lat) {
		result["lat"] = updateLocation.Lat
	} else {
		result["lat"] = exLocation.Lat
	}

	if lo.IsNotEmpty(updateLocation.Lon) {
		result["lon"] = updateLocation.Lon
	} else {
		result["lat"] = exLocation.Lon
	}

	if lo.IsNotEmpty(updateLocation.Country) {
		result["country"] = updateLocation.Country
	} else {
		result["country"] = exLocation.Country
	}

	if lo.IsNotEmpty(updateLocation.City) {
		result["city"] = updateLocation.City
	} else {
		result["city"] = exLocation.City
	}

	if lo.IsNotEmpty(updateLocation.Postcode) {
		result["postcode"] = updateLocation.Postcode
	} else {
		result["postcode"] = exLocation.Postcode
	}

	if lo.IsNotEmpty(updateLocation.Address) {
		result["address"] = updateLocation.Address
	} else {
		result["address"] = exLocation.Address
	}

	return result
}
