package cloudWatch

import "github.com/stretchr/testify/mock"

type CloudWatchMock struct {
	mock.Mock
}

func (c CloudWatchMock) ReportNewHomeMetric() {
	c.On("ReportNewHomeMetric").Return(nil)
}
