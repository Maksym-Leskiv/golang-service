package cloudWatch

import (
	"context"
	"errors"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/cloudwatch"
	"github.com/aws/aws-sdk-go-v2/service/cloudwatch/types"

	log "github.com/sirupsen/logrus"
	"time"
)

type CloudWatchClient interface {
	ReportNewHomeMetric()
}

type CloudWatchClientImpl struct {
	cwc *cloudwatch.Client
}

func NewCloudWatchClient(cfg aws.Config) (*CloudWatchClientImpl, error) {
	client := cloudwatch.NewFromConfig(cfg)

	return &CloudWatchClientImpl{
		cwc: client,
	}, nil

}

func (c CloudWatchClientImpl) ReportNewHomeMetric() {
	input := &cloudwatch.PutMetricDataInput{
		MetricData: []types.MetricDatum{
			{
				MetricName: aws.String("Homes"),
				Value:      aws.Float64(1.0),
				Unit:       types.StandardUnitCount,
				Timestamp:  aws.Time(time.Now()),
			},
		},
		Namespace: aws.String("HomeService"),
	}

	err := errors.New("")
	_, err = c.cwc.PutMetricData(context.TODO(), input)
	if err != nil {
		log.Fatal(err)
	} else {
		log.Debug("Metric data published successfully")
	}
}
