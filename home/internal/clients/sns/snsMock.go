package sns

import (
	"github.com/stretchr/testify/mock"
	"homeservice/internal/models"
)

type SnsMock struct {
	mock.Mock
}

func (s SnsMock) PublishNotification(home models.Home) {
	s.On("PublishNotification", home).Return(nil)
}
