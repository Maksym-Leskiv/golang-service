package sns

import (
	"context"
	"encoding/json"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/sns"
	log "github.com/sirupsen/logrus"
	"homeservice/internal/models"
)

type SnsClient interface {
	PublishNotification(home models.Home)
}

type SnsClientImpl struct {
	client              *sns.Client
	AddressNotification string
}

type SimpleHome struct {
	Username string
	Address  string
}

func NewSnsClient(cfg aws.Config, topicARN string) (*SnsClientImpl, error) {
	client := sns.NewFromConfig(cfg)

	return &SnsClientImpl{
		client:              client,
		AddressNotification: topicARN,
	}, nil
}

func formatNotification(home models.Home) string {
	simpleHome := SimpleHome{
		Username: home.UserID,
		Address:  home.Location.Address,
	}

	jsonMessage, err := json.Marshal(simpleHome)

	if err != nil {
		log.Error(err)
	}

	return string(jsonMessage)
}

func (s SnsClientImpl) PublishNotification(home models.Home) {
	message := formatNotification(home)

	publishInput := &sns.PublishInput{
		TopicArn: aws.String(s.AddressNotification),
		Message:  aws.String(message),
	}

	publishOutput, err := s.client.Publish(context.TODO(), publishInput)

	if err != nil {
		log.Error(err)
	}

	log.Info("Message published. Message ID: ", publishOutput.MessageId)
}
