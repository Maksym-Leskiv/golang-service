package converter

import (
	"homeservice/internal/clients/dynamodb"
	"homeservice/internal/models"
)

func ConvertHome(home models.Home) dynamodb.Home {
	return dynamodb.Home{
		UserID:   home.UserID,
		Profile:  ConvertProfile(home),
		Location: ConvertLocation(home),
	}
}

func ConvertProfile(home models.Home) *dynamodb.Profile {
	return &dynamodb.Profile{
		Year:     home.Profile.Year,
		Area:     home.Profile.Area,
		Bedrooms: home.Profile.Bedrooms,
	}
}

func ConvertLocation(home models.Home) *dynamodb.Location {
	return &dynamodb.Location{
		Lat:      home.Location.Lat,
		Lon:      home.Location.Lon,
		Country:  home.Location.Country,
		City:     home.Location.City,
		Postcode: home.Location.Postcode,
		Address:  home.Location.Address,
	}
}
