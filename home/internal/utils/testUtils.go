package utils

import (
	"context"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"github.com/kinbiko/jsonassert"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"os"
	"strings"
	"testing"
)

type testResolver struct{}

func (t testResolver) ResolveEndpoint(service, region string, options ...interface{}) (aws.Endpoint, error) {
	return aws.Endpoint{
		URL:           "http://localhost:8000",
		SigningRegion: os.Getenv("AWS_REGION"),
	}, nil
}

func AwsConfig() aws.Config {
	awsConfig, err := config.LoadDefaultConfig(context.Background(),
		config.WithRegion(os.Getenv("AWS_REGION")),
		config.WithEndpointResolverWithOptions(testResolver{}),
	)
	if err != nil {
		panic(err)
	}
	return awsConfig
}

func InitTable(t *testing.T) string {
	tableName := fmt.Sprintf("test-homeTable-%s", strings.ReplaceAll(t.Name(), "/", "-"))
	ctx := context.Background()
	dynamoClient := dynamodb.NewFromConfig(AwsConfig())
	primaryKeyName := "uuid"
	dynamoClient.DeleteTable(ctx, &dynamodb.DeleteTableInput{TableName: &tableName})
	_, err := dynamoClient.CreateTable(ctx, &dynamodb.CreateTableInput{
		TableName: &tableName,
		AttributeDefinitions: []types.AttributeDefinition{
			{
				AttributeName: &primaryKeyName,
				AttributeType: types.ScalarAttributeTypeS,
			},
		},
		KeySchema: []types.KeySchemaElement{
			{
				AttributeName: &primaryKeyName,
				KeyType:       types.KeyTypeHash,
			},
		},
		BillingMode: types.BillingModePayPerRequest,
	})
	if err != nil {
		panic(err)
	}
	return tableName
}

func AssertTestCase(t *testing.T, handlerFunc func(req events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error), testCases []struct {
	Description string
	Event       events.APIGatewayProxyRequest
	Expected    events.APIGatewayProxyResponse
}) {
	for _, tc := range testCases {
		t.Run(tc.Description, func(t *testing.T) {
			actual, err := handlerFunc(tc.Event)
			require.NoError(t, err)
			ja := jsonassert.New(t)
			ja.Assertf(actual.Body, tc.Expected.Body)
			assert.Equal(t, tc.Expected.StatusCode, actual.StatusCode)
		})
	}
}
