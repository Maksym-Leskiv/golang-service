package common

const (
	ErrorMethodNotAllowed         = "method not allowed"
	TableNotDefined               = "homeTable not defined"
	TableCreateError              = "Can not create homeTable"
	TopicNotDefined               = "snsArn not defined"
	SnsCreationError              = "Can not create sns"
	CloudWatchClientCreationError = "CloudWatchClientCreationError"
	ErrorGettingContext           = "Context init error"
)

type ErrorBody struct {
	ErrorMsg *string `json:"error,omitempty"`
}
