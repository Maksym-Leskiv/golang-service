package models

import "github.com/google/uuid"

type Profile struct {
	Year     int `json:"year"`
	Area     int `json:"area"`
	Bedrooms int `json:"bedrooms"`
}

type Location struct {
	Lat      int    `json:"lat"`
	Lon      int    `json:"lon"`
	Country  string `json:"country"`
	City     string `json:"city"`
	Postcode string `json:"postcode"`
	Address  string `json:"address"`
}

type Home struct {
	ID       uuid.UUID `json:"id"`
	UserID   string    `json:"user_id"`
	Profile  *Profile  `json:"profile,omitempty"`
	Location *Location `json:"location,omitempty"`
}
