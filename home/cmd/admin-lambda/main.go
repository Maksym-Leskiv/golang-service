package main

import (
	"context"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/config"
	log "github.com/sirupsen/logrus"
	"homeservice/internal/clients/cloudWatch"
	"homeservice/internal/clients/dynamodb"
	"homeservice/internal/clients/sns"
	"homeservice/internal/common"
	"homeservice/internal/handlers"
	"os"
)

var adminHomeHandler handlers.AdminHomeHandler

func init() {
	cfg, err := config.LoadDefaultConfig(context.Background())
	if err != nil {
		panic(common.ErrorGettingContext)
	}
	tableName := os.Getenv("TABLE_NAME")
	if tableName == "" {
		panic(common.TableNotDefined)
	}
	table, err := dynamodb.NewHomeTable(cfg, tableName)
	if err != nil {
		panic(common.TableCreateError)
	}
	topicArn := os.Getenv("TOPIC_ARN")
	if topicArn == "" {
		panic(common.TopicNotDefined)
	}
	sns, err := sns.NewSnsClient(cfg, topicArn)
	if err != nil {
		panic(common.SnsCreationError)
	}
	cw, err := cloudWatch.NewCloudWatchClient(cfg)
	if err != nil {
		panic(common.CloudWatchClientCreationError)
	}
	adminHomeHandler = handlers.AdminHomeHandler{
		Table:      table,
		Sns:        sns,
		CloudWatch: cw,
	}
}

func initLogger() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetLevel(log.DebugLevel)
}

func main() {
	initLogger()
	lambda.Start(handle)
}

func handle(req events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	switch req.HTTPMethod {
	case "GET":
		return adminHomeHandler.GetHomeById(req)
	case "POST":
		return adminHomeHandler.CreateHome(req)
	case "PUT":
		return adminHomeHandler.UpdateHome(req)
	case "DELETE":
		return adminHomeHandler.RemoveHome(req)
	default:
		return adminHomeHandler.UnhandledMethod()
	}
}
