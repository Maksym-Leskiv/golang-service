package main

import (
	"context"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/config"
	log "github.com/sirupsen/logrus"
	"homeservice/internal/clients/cloudWatch"
	"homeservice/internal/clients/dynamodb"
	"homeservice/internal/common"
	"homeservice/internal/handlers"
	"os"
)

var homeHandler handlers.HomeHandler

func init() {
	cfg, err := config.LoadDefaultConfig(context.Background())
	if err != nil {
		panic(common.ErrorGettingContext)
	}
	tableName := os.Getenv("TABLE_NAME")
	if tableName == "" {
		panic(common.TableNotDefined)
	}
	table, err := dynamodb.NewHomeTable(cfg, tableName)
	if err != nil {
		panic(common.TableCreateError)
	}
	cw, err := cloudWatch.NewCloudWatchClient(cfg)
	if err != nil {
		panic(common.CloudWatchClientCreationError)
	}
	homeHandler = handlers.HomeHandler{
		Table:      table,
		CloudWatch: cw,
	}
}

func initLogger() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetLevel(log.DebugLevel)
}

func main() {
	initLogger()
	lambda.Start(handle)
}

func handle(req events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	switch req.HTTPMethod {
	case "GET":
		return homeHandler.GetHome(req)
	case "POST":
		return homeHandler.CreateHome(req)
	default:
		return homeHandler.UnhandledMethod()
	}
}
