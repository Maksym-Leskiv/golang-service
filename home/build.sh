OUT=dist
mkdir -p ${OUT}
rm -rf ${OUT}/*

start_dynamo(){
  DIR=$(pwd)
    if [ ! -f dynamo-local/DynamoDBLocal.jar ]; then
        mkdir dynamo-local
        cd dynamo-local
        curl https://s3.eu-central-1.amazonaws.com/dynamodb-local-frankfurt/dynamodb_local_latest.zip --output dynamodb_local_latest.zip
        unzip dynamodb_local_latest.zip
        cd $DIR
    fi
    cd dynamo-local
    java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb >/dev/null 2>&1 &
    cd $DIR
}

stop_dynamo(){
  kill $(pgrep -f DynamoDBLocal.jar)
}

build() {
    echo building $1
    env GOOS=linux GOARCH=arm64 CGO_ENABLED=0 go build -tags lambda.norpc -o $OUT/bootstrap ./cmd/$1
    zip -j $OUT/$1.zip $OUT/bootstrap
    rm $OUT/bootstrap
}
start_dynamo
trap stop_dynamo EXIT
go test $(go list ./...)

build user-lambda
build admin-lambda
